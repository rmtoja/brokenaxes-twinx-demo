# brokenaxes-twinx-demo

This is just a code snippet. It's just an example of how to use [brokenaxes](https://github.com/bendichter/brokenaxes/) to insert a break in the X axis, and have a second Y axis sharing the same X axis.

I couldn't find a brokenaxes example doing this, so in the spirit of conserving the [wisdom of the ancients](https://xkcd.com/979/), here is my own thing.

## Figure

![Figure_!](/Figure_1.png)

## Code

```python
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.ticker import MultipleLocator
from brokenaxes import brokenaxes
#import matplotlib.gridspec as gridspec

temperature = [25, 1300, 1400, 1500, 1600]
density = [3, 4, 4.5, 5, 5.5]
densityError = [0.3, 0.4, 0.5, 0.5, 0.6]
porosity = [45, 10, 8, 6, 4]
porosityError = [2.5, 2, 1.5, 1, 0.6]

fig = plt.figure(figsize=(7, 7))
bax = brokenaxes(xlims=((0, 50),(1250, 1650)))


# Use brokenaxes to plot one curve
# bax is a list of two axs, one comes before and one after the break
bax.errorbar(temperature, density , yerr=densityError, capsize=2, color='black', label="Density")

# using twinx(), create two new axs, one per ax in the bax list
ax0 = bax.axs[0].twinx()
ax0.errorbar(temperature, porosity, yerr=porosityError, capsize=2, color='black', linestyle="--", label="Porosity") 
ax1 = bax.axs[1].twinx()
ax1.errorbar(temperature, porosity, yerr=porosityError, capsize=2, color='black', linestyle="--", label="Porosity") 

# choosing scales for Y axis, and major and minor ticks
bax.axs[0].set_ylim(bottom=0, top=6.5)
bax.axs[1].set_ylim(bottom=0, top=6.5)
ax0.set_ylim(bottom=0, top=50)
ax1.set_ylim(bottom=0, top=50)
ax1.set_yticks(list(np.arange(0, 51, 5)))
ax1.yaxis.set_minor_locator(MultipleLocator(1))

# do not show some spines, ticks and labels
ax0.spines['right'].set_visible(False)
ax0.tick_params(right=False, labelright=False)
ax1.spines['left'].set_visible(False)

# axis labels
bax.set_xlabel('Sintering Temp/°C', fontsize=10)
bax.set_ylabel(r'Density/$g.cm^-1$', fontsize=10)
ax1.set_ylabel("Porosity/%", fontsize=10)

# one legend to rule them all
# adapted from https://stackoverflow.com/a/10129461
lines, labels = bax.axs[0].get_legend_handles_labels()
lines2, labels2 = ax0.get_legend_handles_labels()
ax0.legend(lines + lines2, labels + labels2, loc="best")

plt.show()
```
